﻿using Pasa.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pasa.Controllers
{
    public class HomeController : Controller
    {
        SalarBlogDBEntities db = new SalarBlogDBEntities();
        // GET: Home
        public ActionResult Index()
        {
            //ViewBag.s = db.MemberUsers.Where(a=>a.Name =="saman").FirstOrDefault();
            var s = db.samanProc("salar").ToList();
            var desrilizeJson = Newtonsoft.Json.JsonConvert.DeserializeObject<List<samanview>>(GetJson());
           
            return View(s);
        }

        private  string GetJson()
        {

            var model = db.samanview.ToList();
            var serilizeJson = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            return serilizeJson;
        }
       public ActionResult Ajaxi(string Name)
        {
            if (db.MemberUsers.Any(a => a.Name == Name))
            {
                var Hast = "Hast";
                return Json(Hast, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var Nist = "Nist";
                return Json(Nist, JsonRequestBehavior.AllowGet);
            }
        }
    }
}