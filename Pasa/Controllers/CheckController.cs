﻿using Pasa.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pasa.Controllers
{
    public class CheckController : Controller
    {
        SalarBlogDBEntities db = new SalarBlogDBEntities();
        // GET: Check
        public ActionResult Check()
            
        {
            var model = db.MemberUsers.ToArray();//.ToList();
            ViewBag.json = GetJson();
            ViewBag.deserialize = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MemberUsers>>(GetJson()).ToArray();
            
            return View(model);
        }

        private string GetJson()
        {
            var model = db.MemberUsers.ToList();
            var serializeJSon = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            return serializeJSon;
        }
    }
}