﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pasa.Models.MemberModels
{
    public class MemberModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}