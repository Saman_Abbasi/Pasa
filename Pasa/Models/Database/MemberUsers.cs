//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pasa.Models.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class MemberUsers
    {
        public MemberUsers()
        {
            this.Blogs = new HashSet<Blogs>();
            this.Roles = new HashSet<Roles>();
        }
    
        public System.Guid MemberUserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    
        public virtual ICollection<Blogs> Blogs { get; set; }
        public virtual ICollection<Roles> Roles { get; set; }
    }
}
